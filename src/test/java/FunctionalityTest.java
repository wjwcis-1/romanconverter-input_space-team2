import org.junit.Test;
import roman.RomanConverter;

import java.util.Random;

import static org.junit.Assert.*;


public class FunctionalityTest {
    //All tests
    @Test
    public void RandomNumForToRomanTest(){
        RomanConverter romanConverter = new RomanConverter();
        Random rand = new Random();
        int x = rand.nextInt(3998) + 1;
        assertNotNull(romanConverter.toRoman(x));
    }
    @Test (expected=IllegalArgumentException.class)
    public void fromRomanInputTest(){
        RomanConverter converter = new RomanConverter();
        converter.fromRoman("iv");
        fail();
    }
    @Test (expected=IllegalArgumentException.class)
    public void toRomanRangeTest(){
        RomanConverter romanConverter = new RomanConverter();
        romanConverter.toRoman(4000);
        romanConverter.toRoman(-1);
        fail();
    }
    @Test (expected=IllegalArgumentException.class)
    public void fromRomanInvalidInputTest(){
        RomanConverter romanConverter = new RomanConverter();
        romanConverter.fromRoman("123");
        fail();
    }
    @Test
    public void fromRomanValidInputValidTest() {
        RomanConverter romanConverter = new RomanConverter();
        assertEquals(romanConverter.toRoman(1), "I");
    }
    @Test
    public void toRomanAllNumTest(){
        RomanConverter romanConverter = new RomanConverter();
        String Str;
        int ToInt;
        for (int i=1; i < 4000; ++i) {
            Str = romanConverter.toRoman(i);
            ToInt = romanConverter.fromRoman(Str);
            assertEquals(i, ToInt);
        }
    }
    @Test
    public void toRomanReturnTest(){
        RomanConverter converter = new RomanConverter();
        Random rand = new Random();
        int x = rand.nextInt(3998) + 1;
        char[] re = converter.toRoman(x).toCharArray();
        for (int i = 0; i < re.length; i++){
            assertTrue(Character.isUpperCase(re[i]));
        }
    }
}
