import org.junit.Test;
import java.util.Random;
import roman.*;

public class InterfaceTest {
//    @Test
//    public void ToRomanTestLess(){
//        int less_1 = -100;
//        RomanConverter romanConverter = new RomanConverter();
//        romanConverter.toRoman(less_1);
//    }
//    @Test
//    public void ToRomanTestEq1(){
//        int eq_1 = 1;
//        RomanConverter romanConverter = new RomanConverter();
//        romanConverter.toRoman(eq_1);
//    }
    @Test
    public void ToRomanTestBetween(){
        Random random = new Random();
        int between1_3999 = random.nextInt(3998) + 1;
        RomanConverter romanConverter = new RomanConverter();
        romanConverter.toRoman(between1_3999);
    }
//    @Test
//    public void ToRomanTestEq3999(){
//        int eq_3999 = 3999;
//        RomanConverter romanConverter = new RomanConverter();
//        romanConverter.toRoman(eq_3999);
//    }
//    @Test
//    public void ToRomanTestGreater(){
//        int greater_3999 = 4500;
//        RomanConverter romanConverter = new RomanConverter();
//        romanConverter.toRoman(greater_3999);
//    }
//    @Test
//    public void FromRomanTestForNull(){
//        RomanConverter romanConverter = new RomanConverter();
//        romanConverter.fromRoman(null);
//    }
    @Test
    public void FromRomanTestForNotNull(){
        RomanConverter romanConverter = new RomanConverter();
        romanConverter.fromRoman("I");
    }
}
